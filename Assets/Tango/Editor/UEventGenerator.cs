﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

class UEventEditorData
{
	public bool ShouldShow;
	public string Status = "Event";
	public bool IsSelected;
	public string EventId;
	public bool IsRepeating;
	public string Interval;
	public string Comment;
}

public class UEventGenerator : EditorWindow
{
	private Vector2 m_scrollPos;
	private string m_selectedFile;
	private string m_filePath;
	private List<UEventEditorData> m_events = new List<UEventEditorData>();
	
	[MenuItem( "Window/Tango/UEvent" )]
	static void Open()
	{
		GetWindow<UEventGenerator>( true, "UEvent Generator", true );
	}
	
	private void OnGUI()
	{
		GUILayout.Space( 15 );
		
		EditorGUILayout.BeginHorizontal();
		
		GUILayout.Label( "File", GUILayout.Width( position.width * 0.035f ) );
		
		m_selectedFile = GUILayout.TextArea( m_selectedFile );
		
		if ( GUILayout.Button( "Save", GUILayout.Width( position.width * 0.15f ) ) )
		{
			if ( string.IsNullOrEmpty( m_selectedFile ) )
			{
				EditorUtility.DisplayDialog( "Warning", "File name cannot be null or empty.", "Close" );
			}
			else
			{
				m_filePath = EditorUtility.SaveFolderPanel( "Save event config.", "", "" );
				GenerateSaveFile( m_filePath );
			}
		}
		
		if ( GUILayout.Button( "Load", GUILayout.Width( position.width * 0.15f ) ) )
		{
			m_filePath = EditorUtility.OpenFilePanel( "Select File", "", "g.cs" );
			
			if ( m_filePath.Length != 0 )
			{
				m_selectedFile = Path.GetFileNameWithoutExtension( m_filePath );
				m_selectedFile = m_selectedFile.Substring( 0, m_selectedFile.Length - 2 );
				string[] lines = File.ReadAllLines( m_filePath );
				
				DigestData( lines );
			}
		}
		
		EditorGUILayout.EndHorizontal();
		
		m_scrollPos = GUILayout.BeginScrollView( m_scrollPos );
		
		GUILayout.Space( 15 );
		
		GUILayout.BeginHorizontal();
		
		GUILayout.Label( "Events", EditorStyles.boldLabel );
		
		GUILayout.EndHorizontal();
		
		GUILayout.BeginVertical();
		
		GUILayout.Space( 5 );
		
		if ( m_events != null )
		{
			int count = m_events.Count;
			
			for ( int i = 0; i < count; ++i )
			{
				GUILayout.BeginHorizontal();
				m_events[i].IsSelected = GUILayout.Toggle( m_events[i].IsSelected, string.Empty, GUILayout.Width( position.width * 0.015f ) );
				m_events[i].Status = ( string.IsNullOrEmpty( m_events[i].EventId ) ? "Unamed" : m_events[i].EventId );
				m_events[i].ShouldShow = EditorGUILayout.Foldout( m_events[i].ShouldShow, m_events[i].Status );
				GUILayout.EndHorizontal();
				
				if ( m_events[i].ShouldShow )
				{
					GUILayout.ExpandHeight( true );
					GUILayout.BeginHorizontal();
					EditorGUILayout.LabelField( "Name", GUILayout.Width( position.width * 0.15f ) );
					m_events[i].EventId = EditorGUILayout.TextField( m_events[i].EventId, GUILayout.Width( position.width * 0.25f ) );
					GUILayout.EndHorizontal();
					
					GUILayout.BeginHorizontal();
					EditorGUILayout.LabelField( "Interval (Seconds)", GUILayout.Width( position.width * 0.15f ) );
					m_events[i].Interval = EditorGUILayout.TextField( m_events[i].Interval, GUILayout.Width( position.width * 0.25f ) );
					GUILayout.EndHorizontal();
					
					GUILayout.BeginHorizontal();
					EditorGUILayout.LabelField( "Repeat", GUILayout.Width( position.width * 0.15f ) );
					m_events[i].IsRepeating = GUILayout.Toggle( m_events[i].IsRepeating, string.Empty, GUILayout.Width( position.width * 0.015f ) );
					GUILayout.EndHorizontal();
					GUILayout.BeginHorizontal();
					
					EditorGUILayout.LabelField( "Comment", GUILayout.Width( position.width * 0.15f ) );
					m_events[i].Comment = GUILayout.TextField( m_events[i].Comment, GUILayout.Width( position.width * 0.25f ) );
					GUILayout.EndHorizontal();
				}
			}
		}
		
		GUILayout.EndVertical();
		
		GUILayout.EndScrollView();
		
		GUILayout.Space( 15 );
		
		GUILayout.BeginHorizontal();
		
		if ( GUILayout.Button( "Add" ) )
		{
			m_events.Add( new UEventEditorData() );
		}
		
		if ( GUILayout.Button( "Remove" ) )
		{
			RemoveSelectedViews();
		}
		
		if ( GUILayout.Button( "Clear" ) )
		{
			m_events.Clear();
		}
		
		if ( GUILayout.Button( "Generate" ) )
		{
			if ( string.IsNullOrEmpty( m_selectedFile ) )
			{
				EditorUtility.DisplayDialog( "Warning", "File name cannot be null or empty.", "Close" );
			}
			else
			{
				m_filePath = EditorUtility.SaveFolderPanel( "Save Generated Events to folder", "", "" );
				GenerateSaveFile( m_filePath );
				GenerateCSharpFile( m_filePath );
			}
		}
		
		GUILayout.EndHorizontal();
	}
	
	private void DigestData( string[] lines )
	{
		if ( m_events != null )
		{
			m_events.Clear();
			
			for ( int i = 0; i < lines.Length; ++i )
			{
				string line = lines[i];
				line = line.Substring( 2, line.Length - 2 );
				
				string[] items = line.Split( ',' );
				
				UEventEditorData eventData = new UEventEditorData()
				{
					EventId = items[1],
					Interval = items[2],// interval
					IsRepeating = ( string.Equals( items[3], "1" ) ? true : false ), // repeating
					Comment = items[4],
				};
				
				m_events.Add( eventData );
			}
		}
	}
	
	private void RemoveSelectedViews()
	{
		int count = m_events.Count;
		
		List<UEventEditorData> newList = new List<UEventEditorData>();
		
		for ( int i = 0; i < count; ++i )
		{
			if ( !m_events[i].IsSelected )
			{
				newList.Add( m_events[i] );
			}
		}
		
		m_events = newList;
	}
	
	private void GenerateSaveFile( string filePath )
	{
		const string format = "//{0},{1},{2},{3},{4}\n";
		const string fileFormat = ".g.cs";
		
		string contents = string.Empty;
		
		for ( int i = 0; i < m_events.Count; ++i )
		{
			string item = string.Format( format, m_events[i].EventId.GetHashCode(), m_events[i].EventId, ( string.IsNullOrEmpty( m_events[i].Interval ) ? "0" : m_events[i].Interval ), m_events[i].IsRepeating ? 1 : 0, m_events[i].Comment );
			contents += item;
		}
		
		File.WriteAllText( filePath + "/" + m_selectedFile + fileFormat, contents );
	}
	
	private void GenerateCSharpFile( string filePath )
	{
		const string fileFormat = ".cs";
		
		string description = "//------------------------------------------------\n" +
		                     "// Auto-generated do not edit\n" +
		                     "//\n" +
		                     "//\n" +
		                     "// Use the Window/Tango/Generate Events...\n" +
		                     "//\n" +
		                     "// Last Updated: {0}\n" +
		                     "//------------------------------------------------\n";
		                     
		description = string.Format( description, System.DateTime.Now );
		
		string contents = description;
		
		string start = "using Tango;\n" +
		               "public partial class UGeneratedEventID\n";
		contents += start;
		
		string startParanthesis = "{\n";
		contents += startParanthesis;
		
		//string formatDefault = "\t\tpublic const string {0} = {1}; \n" +
		//    "\t\tprivate UEvent {2}_INIT = new UEvent({3}); //{4}\n\n";
		
		string formatWithParam = "\t\tpublic const string {0} = {1}; \n" +
		                         "\t\tprivate UEvent {2}_INIT = new UEvent({3},{4},{5}); //{6}\n\n";
		                         
		                         
		for ( int i = 0; i < m_events.Count; ++i )
		{
			string eventName = m_events[i].EventId;
			string eventNameWithQuotation = string.Format( "\"{0}\"", eventName );
			string item = string.Empty;
			int interval = 0;
			
			if ( !string.IsNullOrEmpty( m_events[i].Interval ) && int.TryParse( m_events[i].Interval, out interval ) )
			{
				item = string.Format( formatWithParam,
				                      eventName,
				                      eventNameWithQuotation,
				                      eventName,
				                      eventName,
				                      interval,
				                      m_events[i].IsRepeating ? "true" : "false",
				                      m_events[i].Comment );
			}
			else
			{
				item = string.Format( formatWithParam,
				                      eventName,
				                      eventNameWithQuotation,
				                      eventName,
				                      eventName,
				                      0,
				                      "false",
				                      m_events[i].Comment );
			}
			
			contents += item;
		}
		
		string endParenthesis = "}";
		contents += endParenthesis;
		
		File.WriteAllText( filePath + "/" + m_selectedFile + fileFormat, contents );
	}
}
