﻿using UnityEngine;
using Tango;

public partial class GameHub : MonoBehaviour
{
	[Header( "Game Data" )]
	[SerializeField]
	private int _score = 0;
	
	public int Score
	{
		get
		{
			return _score;
		}
	}
	
	private void OnPlayerGained( IEventContext context )
	{
		ItemEffectData itemEffectData = context as ItemEffectData;
		
		if ( itemEffectData != null )
		{
			_score += itemEffectData.Amount;
		}
	}
	
	private void OnPlayerDamaged( IEventContext context )
	{
		// do nothing for now
	}
}
