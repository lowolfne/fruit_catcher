﻿using UnityEngine;

public partial class GameHub : MonoBehaviour
{
	[Header( "Canvas Controllers" )]
	[SerializeField]
	private ViewController _mainMenuController = null;
	
	[SerializeField]
	private ViewController _gameplayController = null;
	
	[SerializeField]
	private ViewController _gameoverController = null;
	
	// TODO use CSGUI sometime
	
	public void ShowMainMenu()
	{
		if ( _mainMenuController != null )
		{
			_mainMenuController.Show();
		}
	}
	
	public void HideMainMenu()
	{
		if ( _mainMenuController != null )
		{
			_mainMenuController.Hide();
		}
	}
	
	public void ShowGamePlay()
	{
		if ( _gameplayController != null )
		{
			_gameplayController.Show();
		}
	}
	
	public void HideGamePlay()
	{
		if ( _gameplayController != null )
		{
			_gameplayController.Hide();
		}
	}
	
	public void ShowGameover()
	{
		if ( _gameoverController != null )
		{
			_gameoverController.Show();
		}
	}
	
	public void HideGameover()
	{
		if ( _gameoverController != null )
		{
			_gameoverController.Hide();
		}
	}
}
