﻿using System;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Tango;

[Serializable]
public struct GameSettings
{
	public GameObject PlayerPrefab;
	public AudioClip PlayerDeathClip;
	public float PlayerSpeed;
}


public partial class GameHub : MonoBehaviour
{
#pragma warning disable 649

	[Header( "Game Setting" )]
	[SerializeField]
	private GameSettings _gameSettings;
	
#pragma warning restore 649
	
	private static GameHub _instance = null;
	public static GameHub Instance
	{
		get
		{
			return _instance;
		}
	}
	
	[SerializeField]
	private GameObject _playerGameObject;
	public GameObject PlayerGameObject
	{
		get
		{
			return _playerGameObject;
		}
	}
	
	[SerializeField]
	private int _playerHealth = 0;
	
	[Header( "Item Setting" )]
	[SerializeField]
	private float _itemBoundY = 0f;
	
	public float ItemBoundY
	{
		get
		{
			return _itemBoundY;
		}
	}
	
	private void Awake()
	{
		if ( _instance == null )
		{
			_instance = this;
		}
		else if ( _instance != this )
		{
			Destroy( gameObject );
		}
		
		UEventHub.Initialise< UGeneratedEventID >();
	}
	
	private void Start()
	{
		UEventHub.AddListener( UGeneratedEventID.PLAYER_EVENT_CATCH_FRUIT, OnPlayerGained );
		UEventHub.AddListener( UGeneratedEventID.PLAYER_EVENT_CATCH_BOMB, OnPlayerDamaged );
	}
	
	private void Update()
	{
		UEventHub.Loop( Time.deltaTime );
	}
	
	private void OnApplicationQuit()
	{
		UEventHub.RemoveListener( UGeneratedEventID.PLAYER_EVENT_CATCH_FRUIT, OnPlayerGained );
		UEventHub.RemoveListener( UGeneratedEventID.PLAYER_EVENT_CATCH_BOMB, OnPlayerDamaged );
		
		UEventHub.Clear();
	}
	
	public void StartGame()
	{
		_score = 0;
		
		_playerGameObject = Instantiate( _gameSettings.PlayerPrefab );
		_playerGameObject.transform.position = new Vector3( 0, -5f, 0 );
		
		var entity = _playerGameObject.GetComponent<GameObjectEntity>().Entity;
		var entityManager = World.Active.GetExistingManager<EntityManager>();
		entityManager.AddComponentData( entity, new PlayerComponent() );
		entityManager.AddComponentData( entity, new PlayerInputComponent { Move = new float2( 0f, 0f ) } );
		entityManager.AddComponentData( entity, new HealthComponent { Amount = _playerHealth } );
	}
	
	public void EndGame()
	{
		Destroy( _playerGameObject );
	}
}
