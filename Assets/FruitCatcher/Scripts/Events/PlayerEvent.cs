//------------------------------------------------
// Auto-generated do not edit
//
//
// Use the Window/Tango/Generate Events...
//
// Last Updated: 1/22/2019 9:50:18 PM
//------------------------------------------------
using Tango;
public partial class UGeneratedEventID
{
	public const string PLAYER_EVENT_CATCH_FRUIT = "PLAYER_EVENT_CATCH_FRUIT";
	private UEvent PLAYER_EVENT_CATCH_FRUIT_INIT = new UEvent( PLAYER_EVENT_CATCH_FRUIT, 0, false ); //Player catch a fruit
	
	public const string PLAYER_EVENT_CATCH_BOMB = "PLAYER_EVENT_CATCH_BOMB";
	private UEvent PLAYER_EVENT_CATCH_BOMB_INIT = new UEvent( PLAYER_EVENT_CATCH_BOMB, 0, false ); //Player catch a bomb
	
	public const string PLAYER_EVENT_GAMEOVER = "PLAYER_EVENT_GAMEOVER";
	private UEvent PLAYER_EVENT_GAMEOVER_INIT = new UEvent( PLAYER_EVENT_GAMEOVER, 0, false ); //Player died
	
}