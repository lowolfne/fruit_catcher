﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class PlayerInputSystem : ComponentSystem
{
#pragma warning disable 649
	private struct Data
	{
		public readonly int Length;
		public ComponentDataArray<PlayerInputComponent> PlayerInput;
		public SubtractiveComponent<DeadComponent> Dead;
	}
	
	[Inject] private Data data;
#pragma warning restore 649
	
	protected override void OnUpdate()
	{
		for ( var i = 0; i < data.Length; i++ )
		{
			var newInput = new PlayerInputComponent
			{
				Move = new float2( Input.GetAxisRaw( "Horizontal" ), Input.GetAxisRaw( "Vertical" ) )
			};
			
			data.PlayerInput[i] = newInput;
		}
	}
}
