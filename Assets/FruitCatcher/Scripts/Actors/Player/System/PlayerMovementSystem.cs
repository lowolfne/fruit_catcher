﻿using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public class PlayerMovementSystem : ComponentSystem
{
#pragma warning disable 649
	private struct Data
	{
		public readonly int Length;
		public GameObjectArray GameObject;
		public ComponentArray<Rigidbody> Rigidbody;
		[ReadOnly]
		public ComponentDataArray<PlayerInputComponent> PlayerInput;
		public SubtractiveComponent<DeadComponent> Dead;
		
		// add animator
		public ComponentArray<Animator> Animator;
	}
	
	[Inject]
	private Data data;
#pragma warning restore 649
	
	private const string AnimationMoveKey = "IsMoving";
	
	protected override void OnUpdate()
	{
		var speed = 6f; // SurvivalShooterBootstrap.Settings.PlayerMoveSpeed;
		var dt = Time.deltaTime;
		
		for ( var i = 0; i < data.Length; i++ )
		{
			var move = data.PlayerInput[i].Move;
			
			var movement = new Vector3( move.x, 0, 0 );
			movement = movement.normalized * speed * dt;
			
			var position = data.GameObject[i].transform.position;
			var rigidbody = data.Rigidbody[i];
			
			var newPos = new Vector3( position.x, -4f, 0 ) + movement;
			rigidbody.MovePosition( newPos );
			
			if ( movement.x > 0 )
			{
				rigidbody.MoveRotation( Quaternion.LookRotation( Vector3.right ) );
			}
			else if ( movement.x < 0 )
			{
				rigidbody.MoveRotation( Quaternion.LookRotation( -Vector3.right ) );
			}
			
			var animator = data.Animator[i];
			bool isMoving = move.x != 0;
			animator.SetBool( AnimationMoveKey, isMoving );
		}
	}
}
