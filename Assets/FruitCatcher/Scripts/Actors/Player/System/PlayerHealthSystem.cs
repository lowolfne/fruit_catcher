﻿using UnityEngine;
using Unity.Entities;
using Tango;

public class PlayerHealthSystem : ComponentSystem
{
#pragma warning disable 649
	private struct Data
	{
		public readonly int Length;
		public EntityArray Entity;
		public ComponentDataArray<PlayerComponent> Player;
		public ComponentDataArray<DamageComponent> Damaged;
		public ComponentDataArray<HealthComponent> Health;
		public ComponentArray<Animator> Animator;
		//public ComponentArray<AudioSource> AudioSource;
	}
	
	[Inject] private Data data;
#pragma warning restore 649
	
	protected override void OnUpdate()
	{
		var postUpdateCommands = PostUpdateCommands;
		
		for ( int i = 0; i < data.Length; ++i )
		{
			var entity = data.Entity[i];
			
			var damaged = data.Damaged[i];
			var newHealth = data.Health[i].Amount - damaged.Damage;
			data.Health[i] = new HealthComponent { Amount = newHealth };
			
			postUpdateCommands.RemoveComponent<DamageComponent>( entity );
			
			if ( newHealth <= 0 )
			{
				UEventHub.PostEvent( UGeneratedEventID.PLAYER_EVENT_GAMEOVER );
				
				var animator = data.Animator[i];
				animator.SetTrigger( "IsDead" );
				
				postUpdateCommands.AddComponent( entity, new DeadComponent() );
				
				// TODO Make an UEvent for this one
				
				GameHub.Instance.HideGamePlay();
			}
		}
	}
}
