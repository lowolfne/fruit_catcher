﻿using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
	// TODO must be list of items
	[SerializeField]
	private GameObject _item = null;
	public GameObject Item
	{
		get
		{
			return _item;
		}
	}
	
	[SerializeField]
	private float _spawnTime = 3f;
	public float SpawnTime
	{
		get
		{
			return _spawnTime;
		}
	}
	
	[SerializeField]
	private float _itemSpeed = 6f;
	public float ItemSpeed
	{
		get
		{
			return _itemSpeed;
		}
	}
}
