﻿using Unity.Collections;
using Unity.Entities;
using Tango;

public class ItemEffectSystem : ComponentSystem
{
#pragma warning disable 649
	private struct Data
	{
		public readonly int Length;
		public ComponentArray<ItemEffect> ItemEffects;
		public GameObjectArray ItemObjects;
	}
	
	private struct PlayerData
	{
		public GameObjectArray GameObject;
		public EntityArray Entity;
		public ComponentDataArray<PlayerComponent> Player;
		[ReadOnly] public ComponentDataArray<HealthComponent> Health;
	}
	
	[Inject]
	private Data data;
	[Inject]
	private PlayerData playerData;
#pragma warning restore 649
	
	private EntityManager _entityManager;
	
	protected override void OnCreateManager()
	{
		_entityManager = World.GetExistingManager<EntityManager>();
	}
	
	protected override void OnUpdate()
	{
		var postUpdateCommands = PostUpdateCommands;
		
		for ( var i = 0; i < data.Length; i++ )
		{
			var itemEffect = data.ItemEffects[i];
			
			if ( itemEffect != null && itemEffect.IsDamaged )
			{
				var playerHealth = playerData.Health[0].Amount;
				
				var playerEntity = playerData.Entity[0];
				
				if ( playerHealth > 0 && !_entityManager.HasComponent< DamageComponent >( playerEntity ) )
				{
					postUpdateCommands.AddComponent( playerEntity, new DamageComponent
					{
						Damage = itemEffect.Amount
					} );
					
					UEventHub.PostEvent( UGeneratedEventID.PLAYER_EVENT_CATCH_BOMB );
				}
			}
		}
	}
}
