﻿using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;


public class ItemSpawnerSystem : ComponentSystem
{
#pragma warning disable 649
	private struct Data
	{
		public readonly int Length;
		public ComponentArray<ItemSpawner> Spawner;
	}
	
	private struct PlayerData
	{
		public readonly int Length;
		public ComponentDataArray<HealthComponent> Health;
	}
	
	[Inject] private Data data;
	[Inject] private PlayerData playerData;
#pragma warning restore 649
	
	private EntityManager entityManager;
	private List<float> time = new List<float>();
	
	protected override void OnCreateManager()
	{
		entityManager = World.GetExistingManager<EntityManager>();
	}
	
	protected override void OnUpdate()
	{
		if ( playerData.Length > 0 && playerData.Health[0].Amount <= 0 )
		{
#if VERBOSE
			UnityEngine.Debug.Log( "Player has no health" );
#endif
			return;
		}
		
		var dt = Time.deltaTime;
		
		for ( var i = 0; i < data.Length; i++ )
		{
			if ( time.Count < i + 1 )
			{
				time.Add( 0f );
			}
			
			time[i] += dt;
			
			var spawner = data.Spawner[i];
			
			if ( time[i] >= spawner.SpawnTime )
			{
				var enemy = Object.Instantiate( spawner.Item );
				enemy.transform.position = spawner.transform.position;
				var entity = enemy.GetComponent< GameObjectEntity >().Entity;
				entityManager.AddComponentData( entity, new ItemMoveComponent { Speed = spawner.ItemSpeed } );
				time[i] = 0f;
				
				enemy.transform.SetParent( spawner.transform );
			}
		}
	}
}
