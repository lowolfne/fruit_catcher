﻿using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public class ItemMoveSystem : ComponentSystem
{
#pragma warning disable 649
	private struct Data
	{
		public readonly int Length;
		public ComponentDataArray<ItemMoveComponent> ItemMove;
		public GameObjectArray ItemObject;
	}
	
	private struct PlayerData
	{
		public GameObjectArray GameObject;
		[ReadOnly] public ComponentDataArray<HealthComponent> Health;
	}
	
	[Inject]
	private Data data;
	[Inject]
	private PlayerData playerData;
#pragma warning restore 649
	
	protected override void OnUpdate()
	{
		for ( var i = 0; i < data.Length; i++ )
		{
			var item = data.ItemObject[i];
			
			if ( playerData.Health.Length > 0 && playerData.Health[0].Amount > 0 )
			{
				item.transform.position += ( Vector3.down * data.ItemMove[i].Speed * Time.deltaTime );
			}
		}
	}
}
