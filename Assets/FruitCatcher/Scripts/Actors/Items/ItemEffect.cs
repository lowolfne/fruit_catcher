﻿using Tango;
using UnityEngine;

public enum ItemEffectType
{
	Gain,
	Damage,
	Max
}

public class ItemEffectData : IEventContext
{
	public int Amount;
}

public class ItemEffect : MonoBehaviour
{

	[SerializeField]
	private ItemEffectType _itemEffectType = ItemEffectType.Gain;
	public ItemEffectType ItemEffectType
	{
		get
		{
			return _itemEffectType;
		}
	}
	
	[SerializeField]
	private GameObject _explosionEffect = null;
	
	[SerializeField]
	private GameObject _collectEffect = null;
	
	[SerializeField]
	private int _amount = 0;
	public int Amount
	{
		get
		{
			return _amount;
		}
	}
	
	private bool _isDamaged = false;
	public bool IsDamaged
	{
		get
		{
			return _isDamaged;
		}
	}
	
	private void Start()
	{
		// add listeners
		UEventHub.AddListener( UGeneratedEventID.PLAYER_EVENT_CATCH_BOMB, OnHitBomb );
		UEventHub.AddListener( UGeneratedEventID.PLAYER_EVENT_GAMEOVER, OnPlayerDied );
	}
	
	private void Update()
	{
		if ( transform.position.y <= GameHub.Instance.ItemBoundY )
		{
			PlayParticleEffect();
			
			Destroy( gameObject );
		}
	}
	
	private void OnDestroy()
	{
		// remove listners
		UEventHub.RemoveListener( UGeneratedEventID.PLAYER_EVENT_CATCH_BOMB, OnHitBomb );
		UEventHub.RemoveListener( UGeneratedEventID.PLAYER_EVENT_GAMEOVER, OnPlayerDied );
	}
	
	private void OnTriggerEnter( Collider other )
	{
		if ( other.gameObject == GameHub.Instance.PlayerGameObject )
		{
			// player gain points
			if ( _itemEffectType == ItemEffectType.Gain )
			{
				UEventHub.PostEvent( UGeneratedEventID.PLAYER_EVENT_CATCH_FRUIT, new ItemEffectData()
				{
					Amount = _amount
				} );
				
				PlayParticleEffect();
				
				Destroy( gameObject );
			}
			else if ( _itemEffectType == ItemEffectType.Damage )
			{
				// trigger damage
				_isDamaged = true;
			}
		}
	}
	
	private void OnHitBomb( IEventContext context )
	{
		if ( _isDamaged )
		{
			_isDamaged = false;
			
			PlayParticleEffect();
			
			Destroy( gameObject );
		}
	}
	
	private void OnPlayerDied( IEventContext context )
	{
		PlayParticleEffect();
		
		Destroy( gameObject );
	}
	
	private void PlayParticleEffect()
	{
		if ( _explosionEffect == null || _collectEffect == null )
		{
			return;
		}
		
		if ( _itemEffectType == ItemEffectType.Damage )
		{
			Instantiate( _explosionEffect, transform.position, new Quaternion() );
		}
		else
		{
			Instantiate( _collectEffect, transform.position, new Quaternion() );
		}
	}
}
