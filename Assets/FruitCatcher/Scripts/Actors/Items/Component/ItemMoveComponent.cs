﻿using Unity.Entities;

public struct ItemMoveComponent : IComponentData
{
	public float Speed;
}
