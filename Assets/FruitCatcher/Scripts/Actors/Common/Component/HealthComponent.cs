﻿using Unity.Entities;

public struct HealthComponent : IComponentData
{
	public float Amount;
}
