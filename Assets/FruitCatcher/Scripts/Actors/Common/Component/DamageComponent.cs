﻿using Unity.Entities;
using Unity.Mathematics;

public struct DamageComponent : IComponentData
{
	public int Damage;
	public float3 HitPoint;
}
