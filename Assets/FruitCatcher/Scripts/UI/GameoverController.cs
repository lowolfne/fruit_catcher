﻿using UnityEngine;
using UnityEngine.UI;

public class GameoverController : ViewController
{
	[SerializeField]
	private Button _backToMainMenu = null;
	
	[SerializeField]
	private Text _scoreText = null;
	
	private void Start()
	{
		if ( _backToMainMenu != null )
		{
			_backToMainMenu.onClick.AddListener( OnClickBackToMainMenu );
		}
	}
	
	private void OnDestroy()
	{
		if ( _backToMainMenu != null )
		{
			_backToMainMenu.onClick.RemoveListener( OnClickBackToMainMenu );
		}
	}
	
	private void OnClickBackToMainMenu()
	{
		Hide();
		
		// TODO Make an UEvent for this one
		
		GameHub.Instance.EndGame();
		
		GameHub.Instance.ShowMainMenu();
	}
	
	#region[ IViewController ]
	
	public override void Show()
	{
		base.Show();
		
		if ( _animator != null )
		{
			_animator.SetTrigger( "Show" );
		}
		
		
		if ( _scoreText != null )
		{
			_scoreText.text = GameHub.Instance.Score.ToString();
		}
	}
	
	public override void Hide()
	{
		base.Hide();
		
		if ( _animator != null )
		{
			_animator.SetTrigger( "Hide" );
		}
	}
	
	
	#endregion
}
