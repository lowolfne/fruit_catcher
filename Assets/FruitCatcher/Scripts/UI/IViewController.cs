﻿public interface IViewController
{
	void OnAnimateEnd();
	void OnAnimateIn();
	
	void Show();
	void Hide();
}
