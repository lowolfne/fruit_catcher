﻿using UnityEngine;
using UnityEngine.UI;

public class GameplayController : ViewController
{
	[SerializeField]
	private Text _scoreText;
	
	#region [ Unity methods ]
	
	private void Update()
	{
		if ( _scoreText != null )
		{
			_scoreText.text = GameHub.Instance.Score.ToString();
		}
	}
	
	#endregion
	
	#region[ IViewController ]
	
	public override void Show()
	{
		base.Show();
		
		if ( _animator != null )
		{
			_animator.SetTrigger( "Show" );
		}
		
		if ( _scoreText != null )
		{
			_scoreText.text = "0";
		}
	}
	
	public override void Hide()
	{
		base.Hide();
		
		if ( _animator != null )
		{
			_animator.SetTrigger( "Hide" );
		}
		
		// TODO Make an UEvent for this one
		
		GameHub.Instance.ShowGameover();
	}
	
	
	#endregion
}
