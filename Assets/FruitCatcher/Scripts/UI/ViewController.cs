﻿using UnityEngine;

public class ViewController : MonoBehaviour, IViewController
{
	[SerializeField]
	protected Animator _animator;
	
	public virtual void OnAnimateIn()
	{
	}
	
	public virtual void OnAnimateEnd()
	{
	}
	
	public virtual void Show()
	{
	}
	
	public virtual void Hide()
	{
	}
}
