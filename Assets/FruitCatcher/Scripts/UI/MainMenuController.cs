﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : ViewController
{
	[SerializeField]
	private Button _startButton = null;
	
	[SerializeField]
	private Button _quitButton = null;
	
	private void Start()
	{
		// TODO Make an UEvent for this one
		
		GameHub.Instance.ShowMainMenu();
		
		if ( _startButton != null )
		{
			_startButton.onClick.AddListener( OnClickStart );
		}
		
		if ( _quitButton != null )
		{
			_quitButton.onClick.AddListener( OnClickQuit );
		}
	}
	
	private void OnDestroy()
	{
		if ( _startButton != null )
		{
			_startButton.onClick.RemoveListener( OnClickStart );
		}
		
		if ( _quitButton != null )
		{
			_quitButton.onClick.RemoveListener( OnClickQuit );
		}
	}
	
	private void OnClickStart()
	{
		Hide();
		
		// TODO Make an UEvent for this one
		
		GameHub.Instance.ShowGamePlay();
		
		GameHub.Instance.StartGame();
	}
	
	private void OnClickQuit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}
	
	#region[ IViewController ]
	
	public override void OnAnimateEnd()
	{
	}
	
	public override void Show()
	{
		base.Show();
		
		if ( _animator != null )
		{
			_animator.SetTrigger( "Show" );
		}
	}
	
	public override void Hide()
	{
		base.Hide();
		
		if ( _animator != null )
		{
			_animator.SetTrigger( "Hide" );
		}
	}
	
	
	#endregion
}
